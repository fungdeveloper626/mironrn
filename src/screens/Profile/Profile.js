import {ProfileFlatListItem} from '@/components';
import {PROFILEFLATLISTDATA} from '@/constants';
import {strings} from '@/localization';
import {styles} from '@/screens/Profile/Profile.styles';
import {TextStyles} from '@/theme';
import {useNavigation, useTheme} from '@react-navigation/native';
import React from 'react';
import {FlatList, Text, View} from 'react-native';

export function Profile() {
  const {colors} = useTheme();
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <Text style={[TextStyles.title, styles.title, {color: colors.text}]}>
        {strings.profile.message}
      </Text>
      <FlatList
        data={PROFILEFLATLISTDATA}
        keyExtractor={item => item.id}
        renderItem={({item}) => {
          return (
            <ProfileFlatListItem
              item={item}
              onPress={() => {
                navigation.navigate('ProfileDetails', {
                  item: item,
                });
              }}
            />
          );
        }}
      />
    </View>
  );
}
