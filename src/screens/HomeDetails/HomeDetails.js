import {styles} from '@/screens/HomeDetails/HomeDetails.styles';
import {TextStyles} from '@/theme';
import {useTheme} from '@react-navigation/native';
import React from 'react';
import {Text, View} from 'react-native';

export function HomeDetails(props) {
  const {colors} = useTheme();
  const item = props.route.params.item;
  return (
    <View style={styles.container}>
      <Text style={[TextStyles.title, {color: colors.text}]}>{item.title}</Text>
      <Text>{item.body}</Text>
    </View>
  );
}
