export {Home} from '@/screens/Home/Home';
export {HomeDetails} from '@/screens/HomeDetails/HomeDetails';
export {Profile} from '@/screens/Profile/Profile';
export {ProfileDetails} from '@/screens/ProfileDetails/ProfileDetails';
