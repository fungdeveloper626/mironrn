import {HomeFlatListItem} from '@/components';
import {HOMEFLATLISTDATA} from '@/constants';
import {styles} from '@/screens/Home/Home.styles';
import {useNavigation, useTheme} from '@react-navigation/native';
import React from 'react';
import {FlatList, View} from 'react-native';

export function Home() {
  const {colors} = useTheme();
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <FlatList
        data={HOMEFLATLISTDATA}
        keyExtractor={item => item.id}
        renderItem={({item}) => {
          return (
            <HomeFlatListItem
              item={item}
              onPress={() => {
                navigation.navigate('HomeDetails', {
                  item: item,
                });
              }}
            />
          );
        }}
      />
    </View>
  );
}
