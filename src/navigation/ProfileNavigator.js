import {NAVIGATION} from '@/constants';
import {Profile, ProfileDetails} from '@/screens';
import React from 'react';
import {createNativeStackNavigator} from 'react-native-screens/native-stack';

const Stack = createNativeStackNavigator();

export function ProfileNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={NAVIGATION.profile}
        component={Profile}
        options={{headerLargeTitle: true}}
      />
      <Stack.Screen
        name={NAVIGATION.profileDetails}
        component={ProfileDetails}
      />
    </Stack.Navigator>
  );
}
