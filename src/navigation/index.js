import {AppNavigator} from '@/navigation/AppNavigator';
import {theme} from '@/theme';
import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {useColorScheme} from 'react-native';

export function RootNavigator() {
  const scheme = useColorScheme();

  return (
    <NavigationContainer theme={theme[scheme]}>
      <AppNavigator />
    </NavigationContainer>
  );
}
