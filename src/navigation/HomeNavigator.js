import {NAVIGATION} from '@/constants';
import {Home, HomeDetails} from '@/screens';
import React from 'react';
import {createNativeStackNavigator} from 'react-native-screens/native-stack';

const Stack = createNativeStackNavigator();

export function HomeNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name={NAVIGATION.home} component={Home} />
      <Stack.Screen name={NAVIGATION.homeDetails} component={HomeDetails} />
    </Stack.Navigator>
  );
}
