import {RootNavigator} from '@/navigation';
import React from 'react';
import {enableScreens} from 'react-native-screens';

enableScreens();

export function App() {
  return <RootNavigator />;
}
