import {TextStyles} from '@/theme';
import {useTheme} from '@react-navigation/native';
import PropTypes from 'prop-types';
import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {Card} from 'react-native-elements';

export function HomeFlatListItem({item, onPress}) {
  const {colors} = useTheme();
  return (
    <Card>
      <TouchableOpacity onPress={onPress}>
        <Text style={[TextStyles.title, {color: colors.text}]}>
          {item.title}
        </Text>
        <Text>{item.body}</Text>
      </TouchableOpacity>
    </Card>
  );
}

HomeFlatListItem.propTypes = {
  item: PropTypes.object.isRequired,
};
