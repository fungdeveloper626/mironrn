module.exports = {
  env: {
    jest: true,
  },
  root: true,
  extends: ['@react-native-community'],
  plugins: ['module-resolver'],
  rules: {
    'comma-dangle': ['error', 'only-multiline'],
    'import/newline-after-import': 'error',
    'import/order': 'error',
    'module-resolver/use-alias': 'error',
    'no-console': 'warn',
  },
  settings: {
    'import/ignore': ['node_modules/react-native/index\\.js$'],
    'import/resolver': {
      'babel-module': {},
    },
  },
};
