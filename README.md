# Specification and Environment

Android Simulator: Nexus_5X_API_27(AVD) - 8.1.0
Node Version: v15.12.0

## Installation

Use npm to install the package.

```bash
npm install
```
## Usage

Run the app

```bash
npx react-native run-android
```